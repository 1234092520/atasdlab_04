﻿using System;
using System.Collections.Generic;

class ExpressionCalculator
{
    static void Main(string[] args)
    {
        Console.Write("Введіть вираз: ");
        string input = Console.ReadLine()?.Trim();
        if (input != null)
        {
            Console.WriteLine($"Результат: {CalculateExpression(input)}");
        }
        else
        {
            Console.WriteLine("Не введено жодного значення.");
        }
    }

    static double CalculateExpression(string input)
    {
        var values = input.ToLower().Split(' ');
        var stack = new Stack<double>();

        foreach (var value in values)
        {
            if (double.TryParse(value, out double num))
            {
                stack.Push(num);
            }
            else
            {
                var num1 = stack.Pop();
                var num2 = stack.Pop();

                double result;
                switch (value)
                {
                    case "+":
                        result = num2 + num1;
                        break;
                    case "-":
                        result = num2 - num1;
                        break;
                    case "*":
                        result = num2 * num1;
                        break;
                    case "/":
                        result = num1 != 0 && num2 != 0 ? num2 / num1 : throw new DivideByZeroException("Ділення на нуль");
                        break;
                    case "^":
                        result = Math.Pow(num2, num1);
                        break;
                    case "sqrt":
                        result = Math.Sqrt(num1);
                        break;
                    default:
                        throw new ArgumentException("Недійсний оператор");
                }

                stack.Push(result);
            }
        }

        return stack.Pop();
    }
}
