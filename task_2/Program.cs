﻿using System;

public class Node<T>
{
    public T Data { get; set; }
    public Node<T> Next { get; set; }

    public Node(T data)
    {
        Data = data;
        Next = null;
    }
}

public class Linked<T>
{
    private Node<T> head;
    private int count;

    public Linked()
    {
        head = null;
        count = 0;
    }

    public void PushBack(T data)
    {
        Node<T> newNode = new Node<T>(data);
        if (head == null)
        {
            head = newNode;
        }
        else
        {
            Node<T> current = head;
            while (current.Next != null)
            {
                current = current.Next;
            }
            current.Next = newNode;
        }
        count++;
    }

    public void PushFront(T data)
    {
        Node<T> newNode = new Node<T>(data);
        newNode.Next = head;
        head = newNode;
        count++;
    }

    public void Insert(int index, T data)
    {
        if (index < 0 || index > count)
        {
            Console.WriteLine("Недійсний індекс.");
            return;
        }

        if (index == 0)
        {
            PushFront(data);
            return;
        }

        Node<T> newNode = new Node<T>(data);
        Node<T> current = head;
        for (int i = 0; i < index - 1; i++)
        {
            current = current.Next;
        }
        newNode.Next = current.Next;
        current.Next = newNode;
        count++;
    }

    public T GetByIndex(int index)
    {
        if (index < 0 || index >= count)
        {
            Console.WriteLine("Недійсний індекс.");
            return default(T);
        }

        Node<T> current = head;
        for (int i = 0; i < index; i++)
        {
            current = current.Next;
        }
        return current.Data;
    }

    public void EraseByIndex(int index)
    {
        if (index < 0 || index >= count)
        {
            Console.WriteLine("Недійсний індекс.");
            return;
        }

        if (index == 0)
        {
            head = head.Next;
            count--;
            return;
        }

        Node<T> current = head;
        for (int i = 0; i < index - 1; i++)
        {
            current = current.Next;
        }
        current.Next = current.Next.Next;
        count--;
    }

    public void PrintList()
    {
        Node<T> current = head;
        while (current != null)
        {
            Console.Write(current.Data + " ");
            current = current.Next;
        }
        Console.WriteLine();
    }

    public int Length()
    {
        return count;
    }

    public void Clear()
    {
        head = null;
        count = 0;
    }
}

class Program
{
    static readonly string inputIndex = "Введіть індекс: ";
    static readonly string inputValue = "Введіть значення: ";

    static void FillList(Linked<double> list)
    {
        Random rand = new Random();
        for (int i = 0; i < 10; ++i)
        {
            double randomNumber = rand.Next(-10, 11);
            list.PushBack(randomNumber);
        }
    }

    static void Main(string[] args)
    {
        Linked<double> list = new Linked<double>();
        FillList(list);
        int choice;
        do
        {
            Console.WriteLine("\nМеню:\n");
            Console.WriteLine("1. Вивести список\n");
            Console.WriteLine("2. Додати елемент на початок\n");
            Console.WriteLine("3. Додати елемент в кінець\n");
            Console.WriteLine("4. Додати за індексом (index value)\n");
            Console.WriteLine("5. Знайти за індексом\n");
            Console.WriteLine("6. Видалити за індексом\n");
            Console.WriteLine("7. Видалити перший елемент\n");
            Console.WriteLine("8. Видалити останній елемент\n");
            Console.WriteLine("9. Розмір списку\n");
            Console.WriteLine("10. Очистити список\n");
            Console.WriteLine("0. Вийти\n");
            Console.Write("Виберіть опцію: ");
            choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    {
                        Console.Write("Список: ");
                        list.PrintList();
                        break;
                    }
                case 2:
                    {
                        double data;
                        Console.Write(inputValue);
                        data = double.Parse(Console.ReadLine());
                        list.PushFront(data);
                        break;
                    }
                case 3:
                    {
                        double data;
                        Console.Write(inputValue);
                        data = double.Parse(Console.ReadLine());
                        list.PushBack(data);
                        break;
                    }
                case 4:
                    {
                        int index;
                        double data;
                        Console.Write(inputIndex);
                        index = int.Parse(Console.ReadLine());
                        Console.Write(inputValue);
                        data = double.Parse(Console.ReadLine());
                        list.Insert(index, data);
                        break;
                    }
                case 5:
                    {
                        int index;
                        Console.Write(inputIndex);
                        index = int.Parse(Console.ReadLine());
                        Console.WriteLine("Значення за індексом " + index + ": " + list.GetByIndex(index));
                        break;
                    }
                case 6:
                    {
                        int index;
                        Console.Write(inputIndex);
                        index = int.Parse(Console.ReadLine());
                        list.EraseByIndex(index);
                        break;
                    }
                case 7:
                    {
                        if (list.Length() > 0)
                        {
                            list.EraseByIndex(0);
                            Console.WriteLine("Перший елемент видалено.");
                        }
                        else
                        {
                            Console.WriteLine("Список порожній.");
                        }
                        break;
                    }
                case 8:
                    {
                        if (list.Length() > 0)
                        {
                            list.EraseByIndex(list.Length() - 1);
                            Console.WriteLine("Останній елемент видалено.");
                        }
                        else
                        {
                            Console.WriteLine("Список порожній.");
                        }
                        break;
                    }
                case 9:
                    {
                        Console.WriteLine("Елементів у списку: " + list.Length());
                        break;
                    }
                case 10:
                    {
                        list.Clear();
                        Console.WriteLine("Список очищено.");
                        break;
                    }
                case 0:
                    {
                        Console.WriteLine("Програма завершує роботу...");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                        break;
                    }
            }
        } while (choice != 0);
    }
}
