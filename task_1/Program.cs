﻿using System;

public class Node<T>
{
    public T Data { get; set; }
    public Node<T> Prev { get; set; }
    public Node<T> Next { get; set; }

    public Node(T data)
    {
        Data = data;
        Prev = null;
        Next = null;
    }
}

public class Linked<T>
{
    public Node<T> Head { get; private set; }
    public Node<T> Tail { get; private set; }
    public int Size { get; private set; }

    public Linked()
    {
        Head = null;
        Tail = null;
        Size = 0;
    }

    ~Linked()
    {
        Clear();
    }

    public Node<T> PushFront(T data)
    {
        Node<T> newNode = new Node<T>(data)
        {
            Next = Head
        };
        if (Head != null)
            Head.Prev = newNode;
        else
            Tail = newNode;
        Head = newNode;
        Size++;
        return newNode;
    }

    public Node<T> PushBack(T data)
    {
        Node<T> newNode = new Node<T>(data)
        {
            Prev = Tail
        };
        if (Tail != null)
            Tail.Next = newNode;
        else
            Head = newNode;
        Tail = newNode;
        Size++;
        return newNode;
    }

    public void PopFront()
    {
        if (Head == null) return;
        Head = Head.Next;
        if (Head != null)
            Head.Prev = null;
        else
            Tail = null;
        Size--;
    }

    public void PopBack()
    {
        if (Tail == null) return;
        Tail = Tail.Prev;
        if (Tail != null)
            Tail.Next = null;
        else
            Head = null;
        Size--;
    }

    public Node<T> GetAt(int index)
    {
        Node<T> current = Head;
        int i = 0;

        while (i != index)
        {
            if (current == null)
                return null;
            current = current.Next;
            i++;
        }
        return current;
    }

    public Node<T> this[int index] => GetAt(index);

    public Node<T> Insert(int index, T data)
    {
        Node<T> right = GetAt(index);
        if (right == null)
            return PushBack(data);
        Node<T> left = right.Prev;
        if (left == null)
            return PushFront(data);
        Node<T> newNode = new Node<T>(data)
        {
            Prev = left,
            Next = right
        };
        left.Next = newNode;
        right.Prev = newNode;
        Size++;
        return newNode;
    }

    public void GetByIndex(int index)
    {
        Node<T> current = GetAt(index);
        if (current != null)
            Console.WriteLine(current.Data);
        else
            Console.WriteLine($"Index {index} not found.");
    }

    public void EraseByIndex(int index)
    {
        Node<T> current = GetAt(index);
        if (current == null)
        {
            Console.WriteLine($"Index {index} not found.");
            return;
        }
        Node<T> left = current.Prev;
        Node<T> right = current.Next;
        if (left != null)
            left.Next = right;
        else
            Head = right;
        if (right != null)
            right.Prev = left;
        else
            Tail = left;
        Size--;
    }

    public void Clear()
    {
        while (Head != null)
        {
            Node<T> current = Head;
            Head = Head.Next;
            current = null;
        }
        Tail = null;
        Size = 0;
    }

    public int Length() => Size;

    public void PrintList()
    {
        if (Size == 0)
        {
            Console.WriteLine("List is empty.");
            return;
        }

        for (Node<T> current = Head; current != null; current = current.Next)
            Console.Write($"{current.Data} ");
        Console.WriteLine();
    }
}

class Program
{
    static void Main()
    {
        Linked<int> list = new Linked<int>();
        list.PushBack(1);
        list.PushBack(2);
        list.PushBack(3);
        list.PrintList();

        list.PopFront();
        list.PrintList();

        list.Insert(1, 4);
        list.PrintList();

        list.GetByIndex(1);

        list.EraseByIndex(1);
        list.PrintList();

        list.Clear();
        list.PrintList();
    }
}
